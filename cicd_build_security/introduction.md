# CI/CD and Build Security
***Learn about CI/CD and build principles to safeguard your pipelines.***
![alt text](images/image-10.png)
Welcome to the CI/CD and Build Security network! In this room, we will explore what it takes to secure a DevOps pipeline and the builds it produces. Understanding the potential risks and consequences of insecure build processes is essential for recognising the urgency of implementing robust security measures. In this network, we will explore common insecurities and how threat actors can exploit these to compromise not only the process, but also production systems!

Pre-Requisites

- SDLC
- SSDLC
- Intro to Pipeline Automation
- Dependency Management
  

Learning Objectives

- Understand the significance of CI/CD and build system security within the DevSecOps pipeline.
- Explore the potential risks and consequences associated with insecure CI/CD pipelines and build processes.
- Gain awareness of the importance of integrating robust security measures into the build processes to ensure integrity with the deployment of applications.
- Learn about the practical attacks that can happen against misconfigured CI/CD pipelines and build processes.
  

