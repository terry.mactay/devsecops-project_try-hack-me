# Basic Build

## Mainframe Timer

This repo contains the code for the mainframe timer. This application allows employees to authenticate to the timekeep server for record keeping.

As this is a test application, once the application is running, use the default credentials of `admin:admin` to authenticate for the first time.
